"OddsRatio"	"Count"	"Size"	"Pvalue"	"ModuleE$Term"
14	8	83	3.1e-07	"retrograde vesicle-mediated transport, Golgi to ER"
2.3	66	5000	1.1e-06	"transport"
6.5	12	260	1.3e-06	"response to endoplasmic reticulum stress"
5.5	13	330	2.6e-06	"Golgi vesicle transport"
2.3	66	5100	2.8e-06	"establishment of localization"
4.4	15	480	7.3e-06	"neutrophil activation involved in immune response"
4.4	15	480	7.3e-06	"neutrophil degranulation"
4.3	15	490	8.2e-06	"neutrophil activation"
4.3	15	500	9.3e-06	"granulocyte activation"
4.2	15	500	1e-05	"neutrophil mediated immunity"
4	15	530	2.1e-05	"leukocyte degranulation"
3.9	15	540	2.3e-05	"myeloid cell activation involved in immune response"
3.9	15	540	2.6e-05	"myeloid leukocyte mediated immunity"
2.5	32	1900	3e-05	"vesicle-mediated transport"
3.2	17	740	7.9e-05	"regulated exocytosis"
9.3	6	90	8.1e-05	"ERAD pathway"
7.1	7	140	1e-04	"pyridine nucleotide metabolic process"
7.1	7	140	1e-04	"nicotinamide nucleotide metabolic process"
3.4	15	610	1e-04	"myeloid leukocyte activation"
2.1	44	3200	0.00011	"single-organism localization"
1.9	70	6100	0.00013	"localization"
6.9	7	140	0.00013	"pyridine-containing compound metabolic process"
18	4	33	0.00013	"nucleoside bisphosphate metabolic process"
18	4	33	0.00013	"ribonucleoside bisphosphate metabolic process"
18	4	33	0.00013	"purine nucleoside bisphosphate metabolic process"
17	4	34	0.00015	"NADP metabolic process"
2.1	38	2700	0.00016	"organic substance transport"
250	2	3	0.00019	"regulation of CDP-diacylglycerol-serine O-phosphatidyltransferase activity"
250	2	3	0.00019	"positive regulation of CDP-diacylglycerol-serine O-phosphatidyltransferase activity"
250	2	3	0.00019	"positive regulation of serine C-palmitoyltransferase activity"
6.3	7	150	2e-04	"oxidoreduction coenzyme metabolic process"
32	3	15	0.00021	"protein O-linked mannosylation"
4.3	10	320	0.00023	"coenzyme metabolic process"
3.1	15	680	0.00031	"leukocyte activation involved in immune response"
3.1	15	680	0.00032	"cell activation involved in immune response"
130	2	4	0.00037	"regulation of serine C-palmitoyltransferase activity"
2.8	17	850	0.00042	"exocytosis"
5.5	7	170	0.00047	"ER to Golgi vesicle-mediated transport"
23	3	20	0.00051	"cellular response to misfolded protein"
2.3	23	1400	0.00059	"secretion by cell"
1.8	51	4200	0.00061	"single-organism metabolic process"
20	3	22	0.00068	"protein mannosylation"
2.7	16	820	0.00077	"leukocyte mediated immunity"
19	3	23	0.00077	"response to misfolded protein"
2	32	2300	0.00083	"catabolic process"
2.2	24	1500	0.00085	"secretion"
2.3	21	1300	0.00088	"carbohydrate derivative metabolic process"
10	4	54	0.00089	"ribosome assembly"
7.2	5	94	0.00092	"cellular aldehyde metabolic process"
63	2	6	0.00092	"peptidyl-serine autophosphorylation"
